<%@ page import="java.util.Map" %>
<%@ page import="java.util.Iterator" %><%--
  Created by IntelliJ IDEA.
  User: skwo116
  Date: 10/01/2019
  Time: 11:56 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ex04 form data</title>
</head>
<body>
<h1>Server side response</h1>
<p>Thanks for your submission. The values sent to the server are as follows:</p>
<%
    Map<String, String[]> map = request.getParameterMap();
    Iterator<Map.Entry<String, String[]>> i = map.entrySet().iterator();
%>

<table>



<%    while(i.hasNext()) {
    Map.Entry<String, String[]> entry = i.next();
    String key = entry.getKey(); //.toUpperCase();
    String[] values = entry.getValue();


    if(key.contains("submit") || key.contains("button")) {
    continue;
    }

    int index = key.indexOf("[]");
    if(index != -1) {
    key = key.substring(0, index);
    }
    %>

    <tr>
        <td>
            <%= key %>
        </td>
        <%
            for (String value:values) {
        %>
        <td>
            <%= value %>
        </td>
    <%} %>

    </tr>


<% }%>


</table>

</body>
</html>
