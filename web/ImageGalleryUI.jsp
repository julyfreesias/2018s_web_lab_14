<%--
  Created by IntelliJ IDEA.
  User: skwo116
  Date: 10/01/2019
  Time: 1:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>

</head>
<body>
<table border="1">
    <tr>
        <th>Image</th>
        <th><a href="/ImageGalleryDisplay?sortColumn=filename&order=${filenameSortToggle}ending">File Name
            <img src="images/sort-${filenameSortToggle}.png"></a></th>
        <th><a href="/ImageGalleryDisplay?sortColumn=filesize&order=${filesizeSortToggle}ending">File Size
            <img src="images/sort-${filesizeSortToggle}.png"></a></th>

    </tr>

    <c:forEach items="${fileDataList}" var="fileDatalist">
        <tr>
            <td><img src="/Photos/${fileDatalist.thumbPath.name}"></td>
            <td>${fileDatalist.thumbDisplay}</td>
            <td>${fileDatalist.fullfileSize}</td>
        </tr>
    </c:forEach>

</table>

</body>
</html>
